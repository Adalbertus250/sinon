const mongoose = require("mongoose");

module.exports = {
    init: () => {
        const options = {
            useNewUrlParser: true,
            autoIndex: false,
            useUnifiedTopology: true,
            poolSize: 5,
            connectTimeoutMS: 10000,
            family: 4
        };

        mongoose.connect("mongodb://localhost:27017/Sinon", options);
        mongoose.set("useFindAndModify", false);
        mongoose.Promise = global.Promise;

        mongoose.connection.on("connect", ()=>{
            console.log("Bot je připojen do Databáze");
        });

        mongoose.connection.on("err", err=>{
            console.log(`Nastala chyba: \n ${err}`);
        });

        mongoose.connection.on("disconnect", ()=>{
            console.log("Bot je odpojen od Databáze");
        });
    }
}
