//imports
const {Client, Collection} = require("discord.js")
const fs = require("fs");
require("dotenv-flow").config();

//Definice Clienta
const client = new Client();

//variables
client.commands = new Collection();
client.mongoose = require("./utils/mongoose");
client.config = require("./config");
require("./utils/funkce")(client);

//Event Handler
fs.readdir("./events", (err, files) => {
    if(err) Promise.reject(err);

    files.forEach(file => {
        if(!file.endsWith(".js")) return;
        const event = require(`./events/${file}`);
        let eventName = file.split(".")[0];
        client.on(eventName, event.bind(null, client));
    });
});

//Command Handler
fs.readdir("./commands", (err,files)=> {
    if(err) Promise.reject(err);

    files.forEach(file => {
        if(!file.endsWith(".js")) return;
        let cmdFun = require(`./commands/${file}`);
        let cmdName = file.split(".")[0];
        client.commands.set(cmdName, cmdFun);
    });
})

client.mongoose.init();

client.login(process.env.DiscordToken);