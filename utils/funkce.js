const mongoose = require("mongoose");
const {guildModel, settingsModel} = require("../models/import");

module.exports = client => {
    //Settings
    client.getSettings = async (Settings) => {
        let data = await settingsModel.findOne({guildID: Settings.id});
        if(data) return data
        else return client.config.defaults;
    }

    client.updateSettings = async (Guild, Settings) => {
        let data = await client.getSettings(Guild);

        if(typeof data !== 'object') data = {};
        for(const key in Settings){
            if(Settings.hasOwnProperty(key)){
                if(data[key] !== Settings[key]) data[key] = Settings[key]
                else return;
            }
        }

        return await data.updateOne(Settings);
    }

    client.deleteSettings = async (Settings) => {
        let data = await client.getSettings(Settings);

        return data.deleteOne({ guildID: Settings.id }).then(s => {
            console.log("Nastavení bylo vymazáno");
        })
    }

    client.createSettings = async (Settings) => {
        let data = Object.assign({ _id: mongoose.Types.ObjectId(), guildID: Settings.id, prefix: client.config.defaults.prefix });

        const newSettings = await settingsModel(data);
        return newSettings.save()
            .then(s => {
                console.log(`Defaultní nastavení bylo nastaveno`);
            });

    }
    //Guilds
    client.getGuild = async (Guild) => {
        let data = await guildModel.findOne({guildID: Guild.id});
        if(data) return data
        else return "";
    }

    client.updateGuild = async (Guild, Information) => {
        let data = await client.getGuild(Guild);

        if(typeof data !== 'object') data = {};
        for(const key in Information){
            if(Information.hasOwnProperty(key)){
                if(data[key] !== Information[key]) data[key] = Information[key]
                else return;
            }
        }

        return await data.updateOne(Information);
    }

    client.deleteGuild = async (Guild) => {
        let data = await client.getGuild(Guild);

        return data.deleteOne({ guildID: Guild.id }).then(s => {
            console.log("Guilda byla vymazána!");
        })
    }

    client.createGuild = async (Guild) => {
        let data = Object.assign({ _id: mongoose.Types.ObjectId(), guildID: Guild.id, guildName: Guild.name, guildOwnerID: Guild.ownerID, guildIconURL: Guild.iconURL});

        const newGuild = await guildModel(data);
        return newGuild.save()
            .then(s => {
                console.log(`Guilda byla přidaná!`);
            });

    }
}