module.exports = (client, guild) => {
    client.deleteSettings(guild);
    client.deleteGuild(guild);
}