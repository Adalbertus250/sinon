const typing = require("../helpers/typing");

module.exports.run = (client, message, args, settings) => {
    const embed = require("../embeds/help")(client, args, settings);

    typing(client, message, embed);
}

module.exports.help = (client, message, args, settings) => {
    
}