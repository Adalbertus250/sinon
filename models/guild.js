const mongoose = require("mongoose");

const guildSchema = mongoose.Schema({
    guildID: String,
    guildName: String,
    guildOwnerID: String,
    guildIconURL: String
});

module.exports = mongoose.model("Guilds", guildSchema);