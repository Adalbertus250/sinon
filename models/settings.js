const mongoose = require("mongoose");

const settingsSchema = mongoose.Schema({
    guildID: String,
    prefix: {
        type: String,
        default: "?"
    }
});

module.exports = mongoose.model("Settings", settingsSchema);