const Typing =  require("../helpers/typing");

module.exports.run = async (client, message, args, settings) => {

    let perms = ["MANAGE_GUILD"];
    const NoPerms = require("../embeds/notHavePerms")(client,args,settings,perms);
    const InvalidSettting = require("../embeds/SettingsInvalid")(client,args,settings);
    if(!message.member.hasPermission(perms[0])) {
        await Typing(client, message, NoPerms);
        return;
    };

    const argument = args[0];
    const NewSetting = args.slice(1).join(" ");

    switch(argument){
        case 'prefix':
            if(!NewSetting) {
                Typing(client, message, `**Aktuálně nastavený prefix:** \` ${settings.prefix} \` `);
                return;
            }
            try {
                const Changed = require("../embeds/SettingsChanged")(client,args,settings,argument,NewSetting);

                await client.updateSettings(message.guild, {prefix: NewSetting}).then(s => {
                    Typing(client, message, Changed);
                });
            }catch(e){
                Typing(client,message,`Nastala chyba při zpracování: ${e}`);
            }
            break;
        default:
            Typing(client,message,InvalidSettting);
            break;
    }

}

module.exports.help = async (client, message, args, settings) => {
    let perms = ["MANAGE_GUILD"];
    const NoPerms = require("../embeds/notHavePerms")(client,args,settings,perms);
    if(!message.member.hasPermission(perms[0])) {
        await Typing(client, message, NoPerms);
        return;
    };

    const embed = require("../embeds/SettingsHelp")(client,args,settings);

    await Typing(client,message, embed);
}