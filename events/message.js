module.exports = async (client, message) => {

    let settings;
    try {
        settings = await client.getSettings(message.guild);
    } catch(e) {
        console.error(e);
    }

    if(message.author.bot) return;
    if(message.content.indexOf(settings.prefix) !== 0) return;
    
    const args = message.content.slice(settings.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    const cmd = client.commands.get(command);

    if(!cmd) return;

    if(args[0] == "--help") {
        cmd.help(client, message, args, settings);
        return;
    }

    cmd.run(client, message, args, settings);
}