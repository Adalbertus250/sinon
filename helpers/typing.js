module.exports = (client, message, msg) => {

    message.channel.startTyping();
    setTimeout(() => {
        message.channel.send(msg).then((message) => {
            message.channel.stopTyping();
        });
    }, 2000);

}