module.exports = (client, guildid, name) => {
    let guild = client.guilds.get(guildid);
    return guild.emojis.find(emoji => emoji.name == name);
}